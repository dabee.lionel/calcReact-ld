import React, { Component } from 'react';
import Calculatrice from './Exo/Calculatrice';
import logo from './logo.svg';
import './Css/App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Exercices persos</h1>
        </header>
        <Calculatrice />
      </div>
    );
  }
}

export default App;
