import React,{Component} from 'react';
import '../Css/Calc.css';


class Calculatrice extends Component{
    constructor(){
        super();
        this.state={
            nb1:'',
            nb2:'',
            op:'',
            total:'0'
        }
    }

    stockNb1(event){
        event.preventDefault();
        this.setState({
            nb1: this.state.nb1+=event.target.value
        })
    }

    stockNb2(event){
        event.preventDefault();
        this.setState({
            nb2: this.state.nb2+=event.target.value
        })
    }

    stockOp(event){
        event.preventDefault();
        this.setState({
            op: event.target.value
        })
    }

    calcul(event){
        event.preventDefault();
        switch(this.state.op){
            case '+':
            this.setState({
                total:(parseFloat(this.state.nb1) + parseFloat(this.state.nb2,10)).toFixed(4)
            });
            break;
            case '-':
            this.setState({
                total:(parseFloat(this.state.nb1,10) - parseFloat(this.state.nb2,10)).toFixed(4)
            });
            break;
            case '*':
            this.setState({
                total:(parseFloat(this.state.nb1,10) * parseFloat(this.state.nb2,10)).toFixed(4)
            });
            break;
            case '/':
            this.setState({
                total:(parseFloat(this.state.nb1,10) / parseFloat(this.state.nb2,10)).toFixed(4)
            });
            break;
            default:
            alert('operation error');
        }  
    }

    recupVal(event){
        event.preventDefault();
        if(this.state.op==='')
        {
            this.stockNb1(event);
        }else{
            this.stockNb2(event);
        }    
    }

    clearAll(event){
        event.preventDefault();
        this.setState({
            nb1:'',
            nb2:'',
            op:'',
            total:'0'
        })
    }


    render(){
        return(
            <div className="container">
                <h1>Calculatrice</h1>
                    <div className="calculatrice">
                            <input type="text"
                            name="nombre1" 
                            value={this.state.nb1}
                            onChange={this.stockNb1.bind(this)}
                            autoFocus
                            />
                            <input type="text"
                            name="ope" 
                            value={this.state.op}
                            onChange={this.stockOp.bind(this)}
                            />
                            <input type="text"
                            name="nombre2" 
                            value={this.state.nb2}
                            onChange={this.stockNb2.bind(this)}
                            />
                            <input type="text"
                            name="resultat"
                            value = {this.state.total}
                            />
                        <div className="othersBtn">
                            <button className="fa fa-backspace" onClick={this.clearAll.bind(this)}></button>
                        </div>
                        <div className="clavier">
                            <div className="btns">
                                <div className="rowBtn">
                                    <button value='1' onClick={this.recupVal.bind(this)}>1</button>
                                    <button value='2' onClick={this.recupVal.bind(this)}>2</button>
                                    <button value='3' onClick={this.recupVal.bind(this)}>3</button>
                                </div>
                                <div className="rowBtn">
                                    <button value='4' onClick={this.recupVal.bind(this)}>4</button>
                                    <button value='5' onClick={this.recupVal.bind(this)}>5</button>
                                    <button value='6' onClick={this.recupVal.bind(this)}>6</button>
                                </div>
                                <div className="rowBtn">
                                    <button value='7' onClick={this.recupVal.bind(this)}>7</button>
                                    <button value='8' onClick={this.recupVal.bind(this)}>8</button>
                                    <button value='9' onClick={this.recupVal.bind(this)}>9</button>
                                </div>
                                <div className="rowBtn">
                                    <button value='.' onClick={this.recupVal.bind(this)}>.</button>
                                    <button value='0' onClick={this.recupVal.bind(this)}>0</button>
                                    <button onClick={this.calcul.bind(this)}>=</button>
                                </div>
                            </div>
                            <div className="operator">
                                <button value='+' onClick={this.stockOp.bind(this)}>+</button>
                                <button value='-' onClick={this.stockOp.bind(this)}>-</button>
                                <button value='*' onClick={this.stockOp.bind(this)}>*</button>
                                <button value='/' onClick={this.stockOp.bind(this)}>/</button>
                            </div>
                        </div> 
                    </div>
            </div>
        )
    }
}

export default Calculatrice