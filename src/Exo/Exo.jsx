import React, {Component} from 'react';

class Exo extends Component{
    constructor(){
        super();
        this.state={
            userInput :'',
            infos:''
        };
    }

    registInput(event){

        this.setState({
            userInput : event.target.value
        })
    }

    register(e){
        e.preventDefault();
        this.setState({
            infos:this.state.userInput
        })
    }

    render(){
        return(
            <div>
                <h1>Bienvenue {this.state.infos}</h1>
                <form>

                        <input value={this.state.userInput}
                         type="text" 
                         name="user"
                         placeholder="entre ton prenom"
                         onChange={this.registInput.bind(this)}
                         />
                        <button onClick={this.register.bind(this)}>Envoyer</button>

                </form>
            </div>
        )
    }
}

export default Exo